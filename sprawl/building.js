// building.js
// Representation for an individual building.

let WINDOW_MAX_SPACING = 50;

function Window(x, y, width, height, rng) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;

    this.openAtStart = randomIntInRange(0, 1, rng);
    if (randomIntInRange(0, 1, rng)) {
        do {
            this.openTransition1 = randomFloatInRange(0, 1, rng);
            this.openTransition2 = randomFloatInRange(0, 1, rng);
        } while (Math.abs(this.openTransition1 - this.openTransition2) < 0.2 || Math.abs(this.openTransition1 - this.openTransition2) > 0.8);
    }

    this.eveningLightTransition = DayPeriod.sunsetNightTransition + randomFloatInRange(-1/12, 1/12, rng);
    this.morningLightTransition = DayPeriod.sunriseDayTransition + randomFloatInRange(-1/12, 1/12, rng);
}

Window.prototype.draw = function(context, dayPosition) {
    let windowOpen = this.openAtStart;
    if (this.openTransition1 !== undefined && dayPosition >= this.openTransition1 && dayPosition < this.openTransition2) {
        windowOpen = !windowOpen;
    }

    if (!windowOpen) {
        context.fillStyle = "black";
    } else {
        context.fillStyle = (dayPosition >= this.eveningLightTransition && dayPosition < this.morningLightTransition) ? "yellow" : "white";
    }
    context.fillRect(this.x, this.y, this.width, this.height);

    context.strokeStyle = "rgb(64, 64, 64)";
    context.lineWidth = 1;
    context.strokeRect(this.x, this.y, this.width, this.height);
}

Window.prototype.isLit = function(dayPosition) {
    let windowOpen = this.openAtStart;
    if (this.openTransition1 !== undefined && dayPosition >= this.openTransition1 && dayPosition < this.openTransition2) {
        windowOpen = !windowOpen;
    }

    if (!windowOpen) {
        return false
    }
    return (dayPosition >= this.eveningLightTransition && dayPosition < this.morningLightTransition);
}


function Building(id, rng) {
    this.setUp = function() {
        let baseColor = randomIntInRange(155, 255, rng);
        this.color = "rgb(" +
            clamp(baseColor + randomIntInRange(-8, 8, rng), 0, 255) + "," +
            clamp(baseColor + randomIntInRange(-8, 8, rng), 0, 255) + "," +
            clamp(baseColor + randomIntInRange(-8, 8, rng), 0, 255) + ")"


        let windowWidth, windowHorizCount, windowHorizSpacing = WINDOW_MAX_SPACING;
        while (windowHorizSpacing >= WINDOW_MAX_SPACING) {
            windowWidth = randomIntInRange(20, 50, rng);
            windowHorizCount = randomIntInRange(2, Math.floor(this.width / windowWidth), rng);
            windowHorizSpacing = (this.width - windowWidth * windowHorizCount) / (windowHorizCount + 1);
        }

        let windowHeight, windowVertCount, windowVertSpacing = WINDOW_MAX_SPACING;
        while (windowVertSpacing >= WINDOW_MAX_SPACING) {
            windowHeight = randomIntInRange(20, 50, rng);
            windowVertCount = randomIntInRange(2, Math.floor(this.height / windowHeight), rng);
            windowVertSpacing = (this.height - windowHeight * windowVertCount) / (windowVertCount + 1);
        }

        this.windows = [];
        for (let x = 0; x < windowHorizCount; x++) {
            for (let y = 0; y < windowVertCount; y++) {
                this.windows.push(new Window(
                    windowHorizSpacing + x * (windowWidth + windowHorizSpacing), // x
                    windowVertSpacing + y * (windowHeight + windowVertSpacing), // y
                    windowWidth, windowHeight,
                    rng
                ));
            }
        }
    }


    this.id = id;
    this.width = randomIntInRange(100, 300, rng);
    this.height = randomIntInRange(200, 500, rng);
    this.rng = rng;

    // other properties will be set up the first time the building is drawn
    // unnecessary optimization lmao
    this.color = undefined;
    this.windows = undefined;
}

Building.prototype.draw = function(context, dayPosition, backgroundColor, fadeAmount) {
    if (this.color === undefined) {
        this.setUp();
    }

    // Building structure
    context.fillStyle = this.color;
    context.fillRect(0, 0, this.width, this.height);
    context.strokeStyle = "black";
    context.lineWidth = 2;
    context.strokeRect(0, 0, this.width, this.height);

    // Sort windows into lit and unlit; lit windows don't get the distance fade
    let unlitWindows = [], litWindows = [];
    // Calling this variable "window" leads to bad things
    for (win of this.windows) {
        if (win.isLit(dayPosition)) {
            litWindows.push(win);
        } else {
            unlitWindows.push(win);
        }
    }

    for (win of unlitWindows) {
        win.draw(context, dayPosition);
    }

    // Background fade
    context.save();
    context.globalAlpha = fadeAmount;
    context.fillStyle = backgroundColor;
    context.fillRect(0, 0, this.width, this.height);
    context.restore();

    for (win of litWindows) {
        win.draw(context, dayPosition);
    }
}
