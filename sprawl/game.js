// game.js
// Sprawl setup and base layer code.


//
// GAME PARAMETERS
//

let gameWidth = 960, gameHeight = 540;

let numberOfLayers = 3;
let layerScale = 0.8;


//
// GLOBALS
//

let canvas, canvasContext;

let lastUpdateTime;

let gameController;

let defaults = new JSUserDefaults("sprawl_", {
    speed: 0.5,
    time: 0,
    starfieldOffset: 0,
    startingWeather: "0",
    weatherOffset: 0,
    dayLengthSliderOption: 2
});

//
// INITIALIZATION
//

window.addEventListener("load", init);
function init() {
    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    canvas = canvasThings.canvas;
    canvasContext = canvasThings.context;

    // Replace HTML canvas with new canvas and set its display size
    document.getElementById("canvas").parentNode.replaceChild(canvas, document.getElementById("canvas"));
    canvas.id = "canvas";
    canvas.className = "noselect";
    canvas.style.width = gameWidth + "px";
    canvas.style.height = gameHeight + "px";

    // Create the initial seed data if it doesn't already exist
    // Doing this here instead of as defaults for JSUserDefaults because we want to save these immediately
    if (defaults.get("seed") === null) {
        defaults.set("seed", randomIntInRange(0, Number.MAX_SAFE_INTEGER));
    }
    resetSavedLayerData();

    // Set up the building data
    gameController = new GameController(defaults.get("seed"));

    // Set up the options box
    // Show and hide "buttons" and a layer behind the options box that covers the whole game, so you can click anywhere to dismiss the options
    document.getElementById("showoptions").addEventListener("click", function(e) {
        e.target.style.display = "none";
        document.getElementById("options").style.display = "block";
        document.getElementById("backgroundhideoptions").style.display = "block";
    });
    document.getElementById("hideoptions").addEventListener("click", function(e) {
        document.getElementById("showoptions").style.display = "block";
        document.getElementById("options").style.display = "none";
        document.getElementById("backgroundhideoptions").style.display = "none";
    });
    document.getElementById("backgroundhideoptions").addEventListener("click", function(e) {
        document.getElementById("showoptions").style.display = "block";
        document.getElementById("options").style.display = "none";
        document.getElementById("backgroundhideoptions").style.display = "none";
    });

    // Seed field and randomize button
    document.getElementById("seedfield").value = defaults.get("seed");
    document.getElementById("seedfield").addEventListener("change", function(e) {
        defaults.set("seed", e.target.value);
        resetSavedLayerData(true);
        defaults.set("startingWeather", 0);
        defaults.set("weatherOffset", 0);
        gameController.load(e.target.value);
    });
    document.getElementById("randomseedbutton").addEventListener("click", function(e) {
        let seed = randomIntInRange(0, Number.MAX_SAFE_INTEGER);
        defaults.set("seed", seed);
        document.getElementById("seedfield").value = seed;
        resetSavedLayerData(true);
        defaults.set("startingWeather", 0);
        defaults.set("weatherOffset", 0);
        gameController.load(seed);
    });

    // Day length slider
    document.getElementById("daylengthslider").value = defaults.get("dayLengthSliderOption");
    document.getElementById("daylengthamount").textContent = DAY_LENGTHS[defaults.get("dayLengthSliderOption")].label;
    document.getElementById("daylengthslider").addEventListener("input", function(e) {
        let dayPosition = defaults.get("time") / DAY_LENGTHS[defaults.get("dayLengthSliderOption")].length;
        defaults.set("time", dayPosition * DAY_LENGTHS[e.target.value].length);
        defaults.set("dayLengthSliderOption", e.target.value);
        document.getElementById("daylengthamount").textContent = DAY_LENGTHS[e.target.value].label;
    });

    // Speed slider
    let sign = Math.sign(defaults.get("speed"));
    let magnitude = Math.abs(defaults.get("speed"));
    document.getElementById("speedslider").value = sign * Math.log(magnitude + 1) / Math.log(11);
    document.getElementById("speedamount").textContent = defaults.get("speed").toPrecision(2);
    document.getElementById("speedslider").addEventListener("input", function(e) {
        let speed;
        if (e.target.value == 0) {
            speed = 0;
        } else {
            let sign = Math.sign(e.target.value);
            let magnitude = Math.abs(e.target.value);
            speed = sign * (Math.exp(magnitude * Math.log(11)) - 1);
        }
        gameController.targetSpeed = speed;
        gameController.targetSpeedVelocity = 0;
    });

    // Kick off the draw loop
    window.requestAnimationFrame(draw);
}

function resetSavedLayerData(force = false) {
    for (let i = 0; i < numberOfLayers; i++) {
        if (force || defaults.get("startingBuilding" + i) === null) {
            defaults.set("startingBuilding" + i, "0");
        }
        if (force || defaults.get("offset" + i) === null) {
            // Just some random value so we don't start with every layer synchronized on the left side
            defaults.set("offset" + i, 10000);
        }
    }
}


//
// DRAWING
//

// Creates a canvas and 2D drawing context for that canvas that are set up for HiDPI drawing.
// Be careful not to undo the base scale transform on the canvas context with .resetTransform() or anything
// Also be careful to, when you're compositing canvases with .drawImage(), specify the "base" width and height of the canvas and not the scaled size
function makeScaledCanvas(width, height) {
    let scale = window.devicePixelRatio || 1;

    let canvas = document.createElement("canvas");
    canvas.width = width * scale;
    canvas.height = height * scale;

    let context = canvas.getContext("2d");
    context.scale(scale, scale);
    context.imageSmoothingEnabled = false;

    return {canvas: canvas, context: context};
}

// Draws text with an upper left of (x, y)
// Could also do this with context.textBaseline = "top", but that renders inconsistently between FF and Safari/Chrome
function drawText(context, text, x, y, fontSize) {
    context.fillText(text, Math.round(x), Math.round(y + 3 * fontSize / 4));
}

function draw(timestamp) {
    if (lastUpdateTime === undefined) {
        lastUpdateTime = timestamp;
    }
    let dt = timestamp - lastUpdateTime;
    lastUpdateTime = timestamp;

    // Clear the canvas
    canvasContext.fillStyle = "white";
    canvasContext.fillRect(0, 0, gameWidth, gameHeight);

    // Draw and request another draw
    gameController.draw(timestamp, dt);
    window.requestAnimationFrame(draw);
}
