// gamecontroller.js
// Sprawl main game code.

let SPRINGING_THRESHOLD = 0.001;

let DAY_LENGTHS = [
    {length: 1000 * 10, label: "10 seconds"},
    {length: 1000 * 30, label: "30 seconds"},
    {length: 1000 * 60, label: "1 minute"},
    {length: 1000 * 60 * 5, label: "5 minutes"},
    {length: 1000 * 60 * 10, label: "10 minutes"},
    {length: 1000 * 60 * 30, label: "30 minutes"},
    {length: 1000 * 60 * 60, label: "1 hour"},
    {length: 1000 * 60 * 60 * 24, label: "24 hours"}
]

let DayPeriod = {day: 0, daySunsetTransition: 1/3, sunsetNightTransition: 5/12, night: 1/2, nightSunriseTransition: 5/6, sunriseDayTransition: 11/12};
function dayPeriodForPosition(dayPosition) {
    if (dayPosition < DayPeriod.daySunsetTransition) {
        return DayPeriod.day;
    } else if (dayPosition < DayPeriod.sunsetNightTransition) {
        return DayPeriod.daySunsetTransition;
    } else if (dayPosition < DayPeriod.night) {
        return DayPeriod.sunsetNightTransition;
    } else if (dayPosition < DayPeriod.nightSunriseTransition) {
        return DayPeriod.night;
    } else if (dayPosition < DayPeriod.sunriseDayTransition) {
        return DayPeriod.nightSunriseTransition;
    } else {
        return DayPeriod.sunriseDayTransition;
    }
}

let FADE_FACTOR_DAY = 0.9, FADE_FACTOR_NIGHT = 0.5;


function GameController(seed) {
    this.targetSpeed = undefined;
    this.targetSpeedVelocity = undefined;

    this.starfield = undefined;
    this.weather = undefined;
    this.weatherDrift = undefined;
    this.layers = [];
    this.load(seed);
}

// Game loop hooks
GameController.prototype.draw = function(timestamp, dt) {
    // Tick in-game time
    let dayLength = DAY_LENGTHS[defaults.get("dayLengthSliderOption")].length;
    let time = (defaults.get("time") + dt) % dayLength;
    defaults.set("time", time);

    // Some useful time data
    let dayPosition = time / dayLength;
    let dayPeriod = dayPeriodForPosition(dayPosition);
    let fadeTransitionPosition = (dayPosition > DayPeriod.night ? (1 - dayPosition) : (dayPosition - DayPeriod.daySunsetTransition)) / (DayPeriod.night - DayPeriod.daySunsetTransition); // only sensible for the four transition periods

    // Update the scroll speed
    if (this.targetSpeed != undefined) {
        let springValues = numericSpring(defaults.get("speed"), this.targetSpeedVelocity, this.targetSpeed, Math.PI, 5, dt / 100);
        if (Math.abs(springValues.x - this.targetSpeed) < SPRINGING_THRESHOLD && Math.abs(springValues.v) < SPRINGING_THRESHOLD) {
            springValues.x = this.targetSpeed;
            springValues.v = undefined;
            this.targetSpeed = undefined
        }

        defaults.set("speed", springValues.x);
        document.getElementById("speedamount").textContent = springValues.x.toPrecision(2);
        this.targetSpeedVelocity = springValues.v;
    }

    // Move the weather
    this.weather.move(dt * (defaults.get("speed") + this.weatherDrift));
    defaults.set("startingWeather", this.weather.currentWeatherBlock.id.toString());
    defaults.set("weatherOffset", this.weather.offset);

    // Draw the sky
    let backgroundColor = this.weather.skyColor(dayPosition, dayPeriod);
    backgroundColor = "rgb(" + backgroundColor.r + "," + backgroundColor.g + "," + backgroundColor.b + ")";
    canvasContext.fillStyle = backgroundColor;
    canvasContext.fillRect(0, 0, gameWidth, gameHeight);

    // Move and draw the starfield
    this.starfield.move(dt * defaults.get("speed"));
    defaults.set("starfieldOffset", this.starfield.offset);
    if (dayPeriod != DayPeriod.day) {
        canvasContext.save();
        if (dayPeriod == DayPeriod.night) {
            canvasContext.globalAlpha = 1;
        } else if (dayPeriod != DayPeriod.day) {
            canvasContext.globalAlpha = fadeTransitionPosition;
        }
        this.starfield.draw(canvasContext);
        canvasContext.restore();
    }

    this.weather.backgroundDraw(canvasContext, dayPosition, dayPeriod);

    // Calculate the layer fade factor based on the time of day
    let fadeFactor = FADE_FACTOR_DAY;
    let fadeStartingMultiplier = 0;
    if (dayPeriod == DayPeriod.night) {
        fadeFactor = FADE_FACTOR_NIGHT;
        fadeStartingMultiplier = 1;
    } else if (dayPeriod != DayPeriod.day) {
        fadeFactor = FADE_FACTOR_DAY + fadeTransitionPosition * (FADE_FACTOR_NIGHT - FADE_FACTOR_DAY);
        fadeStartingMultiplier = fadeTransitionPosition;
    }

    // Update each layer's position and draw
    // Done in reverse for z-axis stacking
    for (let i = numberOfLayers - 1; i >= 0; i--) {
        this.layers[i].move(dt * defaults.get("speed"));
        defaults.set("startingBuilding" + i, this.layers[i].buildings[0].id.toString());
        defaults.set("offset" + i, this.layers[i].offset);
        this.layers[i].draw(canvasContext, dayPosition, backgroundColor, 1 - Math.pow(fadeFactor, fadeStartingMultiplier + i));
        this.weather.layerPostDraw(canvasContext, i);
    }

    this.weather.foregroundDraw(canvasContext, dt);
}

GameController.prototype.load = function(seed) {
    // Randomized amounts of daylight
    let dayPeriodLength = randomFloatInRange(1/6, 1/2, new Math.seedrandom(seed + "-dayPeriodLength"));
    DayPeriod.daySunsetTransition = dayPeriodLength;
    DayPeriod.sunsetNightTransition = dayPeriodLength + 1/12;
    DayPeriod.night = dayPeriodLength + 1/6;

    this.starfield = new Starfield(new Math.seedrandom(seed + "-starfield"));
    this.weather = new Weather(defaults.get("startingWeather"), defaults.get("weatherOffset"));
    let weatherDriftRng = new Math.seedrandom(seed + "-weatherDrift");
    this.weatherDrift = randomSign(weatherDriftRng) * randomFloatInRange(0.05, 0.15, weatherDriftRng);
    this.layers = [];
    for (let i = 0; i < numberOfLayers; i++) {
        this.layers.push(new BuildingLayer(seed + "-" + i, defaults.get("startingBuilding" + i), defaults.get("offset" + i), Math.pow(layerScale, i)));
    }
}
