// weather.js
// Representation for weather conditions

let DAY_COLOR = {r: 176, g: 255, b: 255}, TRANSITION_COLOR = {r: 255, g: 176, b: 128}, NIGHT_COLOR = {r: 16, g: 16, b: 16};
function mergeColors(c1, c2, p) {
    return {
        r: c1.r + (c2.r - c1.r) * p,
        g: c1.g + (c2.g - c1.g) * p,
        b: c1.b + (c2.b - c1.b) * p
    }
}


function Cloud() {
    // We'll just use standard Math.random for weather particles because I absolutely don't want to put in the effort to make them deterministic across reloads
    this.height = randomIntInRange(50, 100);
    this.width = randomIntInRange(this.height + 100, 300);
    this.x = 0;
    this.y = randomIntInRange(0, 100);
    this.cullOnLeft = true;

    this.speed = randomFloatInRange(-0.1, 0.1);
    this.opacity = randomFloatInRange(0.8, 1);
}

Cloud.prototype.draw = function(context, color) {
    context.save();
    context.globalAlpha *= this.opacity;
    context.fillStyle = "rgb(" + color.r + "," + color.g + "," + color.b + ")";
    context.beginPath();
    context.rect(this.x + this.height / 2, this.y, this.width - this.height, this.height);
    context.moveTo(this.x + this.height, this.y + this.height / 2);
    context.arc(this.x + this.height / 2, this.y + this.height / 2, this.height / 2, 0, 2 * Math.PI);
    context.moveTo(this.x + this.width, this.y + this.height / 2);
    context.arc(this.x + this.width - this.height / 2, this.y + this.height / 2, this.height / 2, 0, 2 * Math.PI);
    context.fill();
    context.restore();
}

Cloud.prototype.move = function(offset) {
    let deltaX = offset / 20 + this.speed;
    this.x -= deltaX;
    this.cullOnLeft = (deltaX > 0);
}

Cloud.prototype.shouldBeCulled = function() {
    if (this.cullOnLeft) {
        return (this.x < -this.width);
    } else {
        return (this.x > gameWidth);
    }
}

Cloud.prototype.shouldBeCulled = function() {
    if (this.cullOnLeft) {
        return (this.x < -this.width);
    } else {
        return (this.x > gameWidth);
    }
}

Cloud.prototype.isOffscreen = function() {
    return (this.x > gameWidth || this.x < -this.width);
}


let WeatherBlockType = { clear: 0, cloudy: 1, rain: 2, fog: 3 };
function WeatherBlock(id, rng, weatherProbabilities = [0.4, 0.8, 0.9]) {
    // We'll just use standard Math.random for weather particles because I absolutely don't want to put in the effort to make them deterministic across reloads
    this.makeRainParticle = function() {
        return {x: randomIntInRange(0, gameWidth), y: -100, xSpeed: randomFloatInRange(-0.1, 0.1), ySpeed: randomFloatInRange(0.8, 1.2)};
    }

    this.id = id;
    this.transitionWidth = randomIntInRange(1000, 5000, rng);
    this.width = randomIntInRange(1000, 60000, rng) + this.transitionWidth;

    let i = randomFloatInRange(0, 1, rng);
    if (i < weatherProbabilities[0]) {
        this.type = WeatherBlockType.clear;
        this.numberOfClouds = randomIntInRange(0, 2, rng);
    } else if (i < weatherProbabilities[1]) {
        this.type = WeatherBlockType.cloudy;
        this.numberOfClouds = randomIntInRange(3, 8, rng);
    } else if (i < weatherProbabilities[2]) {
        this.type = WeatherBlockType.fog;
        this.numberOfClouds = 0;
    } else {
        this.type = WeatherBlockType.rain;
        this.numberOfClouds = randomIntInRange(5, 10, rng);
        this.rain = [];
        for (let i = 0; i < 100; i++) {
            let rainParticle = this.makeRainParticle();
            rainParticle.y = randomIntInRange(-100, gameHeight + 100);
            this.rain.push(rainParticle);
        }
    }
}

WeatherBlock.prototype.cloudColor = function(dayPosition, dayPeriod) {
    let transitionPosition = 0;
    if (dayPeriod == DayPeriod.night) {
        transitionPosition = 1;
    } else if (dayPeriod == DayPeriod.sunsetNightTransition) {
        transitionPosition = 1 - (DayPeriod.night - dayPosition) * 12;
    } else if (dayPeriod == DayPeriod.nightSunriseTransition) {
        transitionPosition = (DayPeriod.sunriseDayTransition - dayPosition) * 12;
    }

    if (this.type == WeatherBlockType.rain) {
        return mergeColors({r: 160, b: 160, g: 160}, {r: 32, g: 32, b: 32}, transitionPosition);
    }
    return mergeColors({r: 256, g: 256, b: 256}, {r: 64, g: 64, b: 64}, transitionPosition);
}

WeatherBlock.prototype.filteredSkyColor = function(color) {
    if (this.type == WeatherBlockType.rain) {
        return mergeColors(color, {r: 0, g: 0, b: 0}, 0.3);
    }
    return color;
}

WeatherBlock.prototype.transitionPosition = function(offset) {
    if (offset < this.width - this.transitionWidth) {
        return 0;
    } else {
        return (offset - this.width + this.transitionWidth) / this.transitionWidth;
    }
}

WeatherBlock.prototype.backgroundDraw = function(context) {

}

WeatherBlock.prototype.layerPostDraw = function(context, layer) {
    if (this.type == WeatherBlockType.fog) {
        context.save();
        context.globalAlpha = context.globalAlpha * (1 - Math.pow(0.7, layer + 1));
        context.fillStyle = "white";
        context.fillRect(0, 0, gameWidth, gameHeight);
        context.restore();
    }
}

WeatherBlock.prototype.foregroundDraw = function(context, dt, offset) {
    if (this.type == WeatherBlockType.rain) {
        context.strokeStyle = "black";
        context.lineWidth = 1;
        context.beginPath();
        for (let i = 0; i < this.rain.length; i++) {
            let x = (this.rain[i].x + offset) % gameWidth;
            context.moveTo(x, this.rain[i].y);
            context.lineTo(x, this.rain[i].y + 50);

            this.rain[i].x = (this.rain[i].x + dt * this.rain[i].xSpeed) % gameWidth;
            this.rain[i].y += dt * this.rain[i].ySpeed;
            if (this.rain[i].y > gameHeight + 100) {
                this.rain[i] = this.makeRainParticle();;
            }
        }
        context.stroke();
    }
}


function Weather(startingWeather, offset) {
    this.makeWeatherBlock = function(id) {
        return new WeatherBlock(id, new Math.seedrandom(defaults.get("seed") + "-weather-" + id.toString()), this.weatherProbabilities);
    }

    this.draw = function(func, context, ...args) {
        let transitionPosition = this.currentWeatherBlock.transitionPosition(this.offset);
        if (transitionPosition > 0 && this.currentWeatherBlock.type != this.nextWeatherBlock.type) {
            context.save();
            context.globalAlpha = 1 - transitionPosition;
            this.currentWeatherBlock[func](context, ...args);
            context.globalAlpha = transitionPosition;
            this.nextWeatherBlock[func](context, ...args);
            context.restore();
        } else {
            this.currentWeatherBlock[func](context, ...args);
        }
    }

    let id = bigInt(startingWeather);
    this.weatherProbabilities = [];
    let probabilityRng = new Math.seedrandom(defaults.get("seed") + "-weatherProbabilities");
    let dividers = Object.keys(WeatherBlockType).length - 1;
    for (let i = 0; i < dividers; i++) {
        this.weatherProbabilities.push(randomFloatInRange(0, 1, probabilityRng));
    }
    this.weatherProbabilities.sort();

    this.currentWeatherBlock = this.makeWeatherBlock(id);
    this.nextWeatherBlock = this.makeWeatherBlock(id.add(1));
    this.offset = offset;
    this.scrollPosition = 0;

    this.clouds = [];
    for (let i = 0; i < this.currentWeatherBlock.numberOfClouds; i++) {
        let cloud = new Cloud();
        cloud.x = randomIntInRange(-gameWidth - cloud.width, 2 * gameWidth);
        this.clouds.push(cloud);
    }

    this.move(0);
}

Weather.prototype.move = function(offset) {
    this.offset += offset;
    this.scrollPosition = (this.scrollPosition - offset + gameWidth) % gameWidth;

    // Scroll right
    while (this.offset < 0) {
        this.nextWeatherBlock = this.currentWeatherBlock;
        this.currentWeatherBlock = this.makeWeatherBlock(this.nextWeatherBlock.id.subtract(1));
        this.offset += this.currentWeatherBlock.width;
    }

    // Scroll left
    while (this.offset >= this.currentWeatherBlock.width) {
        this.offset -= this.currentWeatherBlock.width;
        this.currentWeatherBlock = this.nextWeatherBlock;
        this.nextWeatherBlock = this.makeWeatherBlock(this.currentWeatherBlock.id.add(1));
    }

    // Move and cull clouds
    for (cloud of this.clouds) {
        cloud.move(offset);
    }
    this.clouds = this.clouds.filter(cloud => !cloud.shouldBeCulled());
    if (this.clouds.length > this.currentWeatherBlock.numberOfClouds) {
        this.clouds = this.clouds.filter(cloud => !cloud.isOffscreen());
    }

    // Spawn new clouds
    while (this.clouds.length < this.currentWeatherBlock.numberOfClouds) {
        let cloud = new Cloud();
        cloud.move(offset);
        if (cloud.x < 0) {
            cloud.x = randomIntInRange(gameWidth, 2 * gameWidth);
        } else {
            cloud.x = -randomIntInRange(0, gameWidth) - cloud.width;
        }
        this.clouds.push(cloud);
    }
}

Weather.prototype.backgroundDraw = function(context, dayPosition, dayPeriod) {
    this.draw("backgroundDraw", context);

    let cloudColor = this.currentWeatherBlock.cloudColor(dayPosition, dayPeriod);
    let transitionPosition = this.currentWeatherBlock.transitionPosition(this.offset);
    if (transitionPosition > 0) {
        cloudColor = mergeColors(cloudColor, this.nextWeatherBlock.cloudColor(dayPosition, dayPeriod), transitionPosition);
    }
    for (cloud of this.clouds) {
        cloud.draw(context, cloudColor);
    }
}

Weather.prototype.layerPostDraw = function(context, layer) {
    this.draw("layerPostDraw", context, layer);
}

Weather.prototype.foregroundDraw = function(context, dt) {
    this.draw("foregroundDraw", context, dt, this.scrollPosition);
}

Weather.prototype.skyColor = function(dayPosition, dayPeriod) {
    let skyColor = DAY_COLOR;
    if (dayPeriod == DayPeriod.daySunsetTransition) {
        let transitionPosition = (dayPosition - DayPeriod.daySunsetTransition) * 12;
        skyColor = mergeColors(DAY_COLOR, TRANSITION_COLOR, transitionPosition);
    } else if (dayPeriod == DayPeriod.sunsetNightTransition) {
        let transitionPosition = (dayPosition - DayPeriod.sunsetNightTransition) * 12;
        skyColor = mergeColors(TRANSITION_COLOR, NIGHT_COLOR, transitionPosition);
    } else if (dayPeriod == DayPeriod.night) {
        skyColor = NIGHT_COLOR;
    } else if (dayPeriod == DayPeriod.nightSunriseTransition) {
        let transitionPosition = (dayPosition - DayPeriod.nightSunriseTransition) * 12;
        skyColor = mergeColors(NIGHT_COLOR, TRANSITION_COLOR, transitionPosition);
    } else if (dayPeriod == DayPeriod.sunriseDayTransition) {
        let transitionPosition = (dayPosition - DayPeriod.sunriseDayTransition) * 12;
        skyColor = mergeColors(TRANSITION_COLOR, DAY_COLOR, transitionPosition);
    }

    let filteredSkyColor = this.currentWeatherBlock.filteredSkyColor(skyColor);
    let transitionPosition = this.currentWeatherBlock.transitionPosition(this.offset);
    if (transitionPosition > 0) {
        filteredSkyColor = mergeColors(filteredSkyColor, this.nextWeatherBlock.filteredSkyColor(skyColor), transitionPosition);
    }
    return filteredSkyColor;
}
