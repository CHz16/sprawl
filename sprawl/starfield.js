// starfield.js
// Representation for a starfield


function Starfield(rng) {
    let canvasThings = makeScaledCanvas(gameWidth, gameHeight);
    this.stars = canvasThings.canvas;
    let numberOfStars = randomIntInRange(100, 150, rng);
    canvasThings.context.fillStyle = "white";
    for (let i = 0; i < numberOfStars; i++) {
        canvasThings.context.beginPath();
        canvasThings.context.arc(randomIntInRange(0, gameWidth, rng), randomIntInRange(0, gameHeight, rng), randomFloatInRange(0.5, 1.5, rng), 0, 2 * Math.PI);
        canvasThings.context.fill();
    }

    this.offset = defaults.get("starfieldOffset");
}

Starfield.prototype.move = function(offset) {
    this.offset = (this.offset - offset / 50) % gameWidth;
}

Starfield.prototype.draw = function(context) {
    context.drawImage(this.stars, this.offset, 0, gameWidth, gameHeight);
    context.drawImage(this.stars, this.offset + gameWidth, 0, gameWidth, gameHeight);
}
