// buildinglayer.js
// Representation for a parallax layer of buildings.


function BuildingLayer(id, startingBuilding, offset, scale) {
    this.makeBuilding = function(id) {
        return new Building(id, new Math.seedrandom(this.id + "-" + id.toString()));
    }

    this.addBuildingsOnTheRight = function() {
        let layerWidth = -this.offset;
        for (building of this.buildings) {
            layerWidth += building.width;
        }
        let buildingId = this.buildings[this.buildings.length - 1].id.add(1);
        while (layerWidth < this.width) {
            let building = this.makeBuilding(buildingId);
            this.buildings.push(building);
            layerWidth += building.width;
            buildingId = buildingId.add(1);
        }
    }


    this.id = id;
    this.offset = offset;
    this.scale = scale;

    this.width = gameWidth / scale;

    this.buildings = [this.makeBuilding(bigInt(startingBuilding))];
    this.addBuildingsOnTheRight();
}

BuildingLayer.prototype.move = function(offset) {
    this.offset += offset;

    if (this.offset < 0) {
        // Scroll right
        while (this.offset < 0) {
            let buildingId = this.buildings[0].id.subtract(1);
            let building = this.makeBuilding(buildingId);
            this.buildings.unshift(building);
            this.offset += building.width;
        }
        let layerWidth = -this.offset;
        let i = 0;
        while (layerWidth < this.width) {
            layerWidth += this.buildings[i].width;
            i += 1;
        }
        this.buildings.splice(i);
    } else {
        // Scroll left
        this.addBuildingsOnTheRight();
        while (this.offset > this.buildings[0].width) {
            this.offset -= this.buildings[0].width;
            this.buildings.shift();
        }
    }
}

BuildingLayer.prototype.draw = function(context, dayPosition, backgroundColor, fadeAmount) {
    context.save();
    context.scale(this.scale, this.scale);
    let x = -this.offset;
    for (building of this.buildings) {
        context.save();
        context.translate(x, gameHeight - building.height + 2);
        building.draw(context, dayPosition, backgroundColor, fadeAmount);
        context.restore();
        x += building.width;
    }
    context.restore();
}
