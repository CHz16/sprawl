// numbers.js
// Some numbery things.

function randomIntInRange(min, max, rng = Math.random) {
    return min + Math.floor((max - min + 1) * rng());
}

function randomSign(rng = Math.random) {
    return randomIntInRange(0, 1, rng) ? -1 : 1;
}

function randomFloatInRange(min, max, rng = Math.random) {
    return min + (max - min) * rng();
}

function clamp(n, min, max) {
    if (n < min) {
        return min;
    } else {
        return Math.min(n, max);
    }
}


/*

https://github.com/TheAllenChou/numeric-springing

MIT License

Copyright (c) 2016 Allen Chou

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

// x: current value
// v: current velocity
// xt: target value
// zeta: damping ratio
// omega: angular frequency
// h: time step
function numericSpring(x, v, xt, zeta, omega, h) {
    let f = 1 + 2 * h * zeta * omega;
    let oo = omega * omega;
    let hoo = h * oo;
    let hhoo = h * hoo;
    let detInv = 1 / (f + hhoo);
    let detX = f * x + h * v + hhoo * xt;
    let detV = v + hoo * (xt - x);
    return {x: detX * detInv, v: detV * detInv};
}
