Sprawl
======

A screensaver for [GAMES MADE QUICK??? FOUR](https://itch.io/jam/games-made-quick-four). Playable here: https://chz.itch.io/sprawl

Everything here is by me and released under MIT (see LICENSE.txt), except:

* The [BigInteger.js library](https://github.com/peterolson/BigInteger.js), released into the public domain (see `sprawl/lib/BigInteger/LICENSE`)
* Numeric springing code by Allen Chou, released under MIT (see `sprawl/numbers.js`)
* The [seedrandom library by David Bau](https://github.com/davidbau/seedrandom), released under MIT (see `sprawl/lib/seedrandom/LICENSE.md`)
